# Algoritmos y programacion en C

Implementacion de algoritmos y distintos programas en C.

Estructuras de datos:

- [Pila](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Estructuras%20de%20datos/TDA%20Pila)
- [Cola](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Estructuras%20de%20datos/TDA%20Cola)
- [Lista](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Estructuras%20de%20datos/TDA%20Lista)
- [Árbol binario de búsqueda](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Estructuras%20de%20datos/TDA%20ABB)
- [Tabla de hash](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Estructuras%20de%20datos/TDA%20Hash)


Juegos:

- [La venganza de Arrya](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/La%20venganza%20de%20Arrya)

Otros:

- [Los siete reinos](https://gitlab.com/P-Jonathan/Algortimos-y-programacion-en-C/tree/master/Los%20Siete%20Reinos%20(GoT))
